﻿using System;
using System.Diagnostics;
using System.Text;

namespace 防火墙阻止自定义应用程序
{
    public class Common
    {
        public static class Cmd
        {
            private static StringBuilder sb = new StringBuilder();

            public static string Run(string command, params string[] args)
            {
                sb.Clear();
                try
                {
                    Process process = new Process();
                    process.StartInfo.FileName = command;
                    process.StartInfo.Arguments = string.Join(" ", args);
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.UseShellExecute = false;
                    process.OutputDataReceived += (sender, e) => sb.Append(e.Data);
                    process.OutputDataReceived += (sender, e) => sb.Append(e.Data);
                    process.ErrorDataReceived += (sender, e) => sb.Append(e.Data);
                    process.Start();
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();
                    process.WaitForExit();
                }
                catch (Exception e)
                {
                    sb.Append(e.Message);
                }
                return sb.ToString();
            }
        }
    }
}